<!DOCTYPE HTML>
<html>

<head>
    <title>The 2ка</title>
    <#include "common/head.ftl">
</head>

<body>

<#include "common/header.ftl">

<section id="banner">
    <div class="inner">

        <header>
            <h1>Welcome to 2ка</h1>
        </header>

        <div class="flex ">
            <div>
                <span class="icon fa-car"></span>
                <h3>Факультеты</h3>
                <p>Факультеты, представленные в The 2ка</p>
            </div>

            <div>
                <span class="icon fa-camera"></span>
                <h3>Специальности</h3>
                <p>Специальности The 2ка</p>
            </div>

            <div>
                <span class="icon fa-bug"></span>
                <h3>Заявления</h3>
                <p>Онлайн подача заявлений в The 2ка</p>
            </div>
        </div>

        <footer>
            <a href="/departments" class="button">Поехали!</a>
        </footer>

    </div>
</section>

<#include "common/main-page-slideshow.ftl">

<#if model.Gafurov??>
    <#if model.Khasik??>
<section id="three" class="wrapper align-center">
        <div class="inner">
            <div class="flex flex-2">
                <article>

                    <div class="image round">
                        <img src="/img/rector-big.jpg" alt="Rector" height="123" width="150"/>
                    </div>

                    <header>
                        <h3>${model.Gafurov.getUser().getFirstName()} ${model.Gafurov.getUser().getSecondName()}</h3>
                    </header>

                    <p>${model.Gafurov.getJob_description()}</p>

                    <footer>
                        <a href="/employee/${model.Gafurov.getId()}" class="button">
                            ${model.Gafurov.getUser().getSecondName()} И.Р.
                        </a>
                    </footer>

                </article>
                <article>

                    <div class="image round">
                        <img src="/img/hasyanov.jpg" alt="Director ITIS" height="150" width="123"/>
                    </div>

                    <header>
                        <h3>${model.Khasik.getUser().getFirstName()} ${model.Khasik.getUser().getSecondName()}</h3>
                    </header>

                    <p>${model.Khasik.getJob_description()}</p>

                    <footer>
                        <a href="/employee/${model.Khasik.getId()}" class="button">
                            ${model.Khasik.getUser().getSecondName()} А. Ф.
                        </a>
                    </footer>

                </article>
            </div>
        </div>
    </section>
    </#if>
</#if>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/chat-bot.ftl">

<#include "common/scripts.ftl">

</body>

</html>