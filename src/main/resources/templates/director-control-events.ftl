<!DOCTYPE HTML>
<html>

<head>
    <title>Мероприятия</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Мероприятия</h2>
    <#if model.events?size != 0>
            <p>Ниже представлены все мероприятия 2ки</p>
            <br/>
        </header>

        <#list model.events as event>
            <div class="row">
                <section class="6u 12u$(medium)">
                    <h2>${event.getTitle()}</h2>
                    <p><strong>${event.getDate()}</strong> - ${event.getTitle()}</p>
                </section>
                <section class="3u 12u(medium)">
                    <h3>Информация</h3>
                    <p>${event.getDescription()}</p>
                </section>
            </div>
                <hr class="major"/>
        </#list>
    <#else>
            <p>К сожалению, в базе данных нет мероприятий</p>
        </header>
    </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>