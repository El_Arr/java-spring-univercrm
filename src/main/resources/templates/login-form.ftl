<!DOCTYPE HTML>
<html>

<head>
    <title>Вход</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<footer id="footer">
    <div class="inner">

        <h3>Вход</h3>

        <form action="/login" method="post">
            <div class="field">
                <label for="username">Имя пользователя</label>
                <input name="username" id="username" type="text" placeholder="Имя пользователя">
            </div>
            <div class="field">
                <label for="password">Пароль</label>
                <input name="password" id="password" type="password" placeholder="Пароль">
            </div>

            <ul class="actions">
                <li><input value="Поехали!" class="button alt" type="submit"></li>
            </ul>
        </form>

        <a href="/register" class="button special">Зарегистрироваться</a>

        <#include "common/copyright.ftl">

    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
