<!DOCTYPE HTML>
<html>

<head>
    <title>Администратор</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">
        <header class="align-center">
            <h2>Функции</h2>
            <p>Ниже представлены все функции, доступные вам</p>
        </header>

        <div class="row">
            <section class="6u 6u$(medium)">
                <h2>Пользователи</h2>
                <p>Изменение данных о пользователях</p>
            </section>
            <section class="3u$ 12u$(medium)">
                <section class="3u$ 6u$(medium)">
                    <a href="/admin/users" class="button">Пользователи</a>
                </section>
            </section>
        </div>

        <hr class="major"/>

        <div class="row">
            <section class="6u 12u$(medium)">
                <h2>Заявки</h2>
                <p>Изменение данных заявок</p>
            </section>
            <section class="3u$ 6u$(medium)">
                <a href="/admin/tickets" class="button">Заявки</a>
            </section>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
