<!DOCTYPE HTML>
<html>

<head>
    <title>Факультеты</title>
    <#include "common/head.ftl">
</head>

<body class="subpage">

<#include "common/header.ftl">

<section id="main" class="wrapper">
    <div class="inner">

        <header class="align-center">
            <h2>Факультеты</h2>
    <#if model.departments?size != 0>
            <p>Ниже представлены все виды факультетов, обитающих в 2ке</p>
            <#include "common/ajax-search-department.ftl">
        </header>

        <#list model.departments as department>
            <div class="row">
                <section class="6u 12u$(medium)">
                    <h2>${department.getName()}</h2>
                    <p><strong>${department.getName()}</strong> - это ${department.getDescription()}</p>
                </section>
                <section class="3u 6u(medium) 12u$(small)">
                    <h3>Контакты</h3>
                    <p>Email: ${department.getEmail()}</p>
                    <p>Телефон: ${department.getPhone_num()}</p>
                </section>
                <section class="3u$ 6u$(medium) 12u$(small)">
                    <h3>Узнать больше:</h3>
                    <a href="/department/${department.getId()}" class="button">${department.getName()}</a>
                </section>
            </div>
            <hr class="major"/>
        </#list>
    <#else>
            <p>К сожалению, в базе данных нет видов факультетов, обитающих в 2ке</p>
        </header>
    </#if>
    </div>
</section>

<footer id="footer">
    <div class="inner">
        <#include "common/copyright.ftl">
    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
