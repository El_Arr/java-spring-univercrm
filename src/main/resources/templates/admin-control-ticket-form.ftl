<!DOCTYPE HTML>
<html>

<head>
    <title>Администратор/Заявления</title>
    <#include "common/head.ftl">
</head>

<body class="subpage"
">

<#include "common/header.ftl">

<footer id="footer">
    <div class="inner">

        <h3>Заявления</h3>
        <br/>

    <#if model.tickets??>
        <#list model.tickets as ticket>

        <h2>Состояние</h2>
        <select name="status" id="status" form="form${ticket.getId()}">
            <option value="${ticket.getStatus().name()}">
                ${ticket.getStatus().name()}
            </option>
            <#list model.statuses as status>
                <#if status == ticket.getStatus().name()>
                    <#continue>
                </#if>
                <option value="${status}">${status}</option>
            </#list>
        </select>
        <br/>

        <form action="/admin/tickets/edit" method="post" id="form${ticket.getId()}">
            <input type="hidden" name="id" id="id" value="${ticket.getId()}">
            <div class="field">
                <label for="title">Заголовок</label>
                <input name="title" id="title" type="text" placeholder="Заголовок" value="${ticket.getTitle()}">
            </div>
            <div class="field">
                <label for="content">Содержимое</label>
                <textarea name="content" id="content" rows="3"
                          placeholder="Содержимое">${ticket.getContent()}</textarea>
            </div>
            <div class="field">
                <label for="answer">Ответ</label>
                <textarea name="answer" id="answer" rows="3" placeholder="Ответ">${ticket.getAnswer()}</textarea>
            </div>
            <input value="Изменить" class="button alt" type="submit">
        </form>

        <form action="/admin/tickets/delete" method="post">
            <input type="hidden" name="id" id="id" value="${ticket.getId()}">
            <input value="Удалить" class="button alt" type="submit">
        </form>
        <hr class="major"/>

        </#list>
    <#else>

        <h2>Заявок нет в базе!</h2>

    </#if>

    <#include "common/copyright.ftl">

    </div>
</footer>

<#include "common/scripts.ftl">

</body>

</html>
