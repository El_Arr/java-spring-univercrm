<!DOCTYPE HTML>
<html>

<head>
    <title>Заявка/Абитуриент</title>
    <#include "common/head.ftl">
    <#include "common/ajax-ticket-enrollee.ftl">
</head>

<#if model.enrollee??>
<body class="subpage" onload="load_ticket_enrollee()">
<#else>
<body class="subpage">
</#if>
<#include "common/header.ftl">

<footer id="footer">
<div class="inner">

    <h3>Подать заявление</h3>

    <#if model.enrollee??>
        <div class="12u$">
            <div class="select-wrapper">
                <select name="ticket" id="ticket"
                        onchange="load_ticket_enrollee()">
                    <option value="0">Подать заявление</option>
                <#if model.tickets??>
                    <#list model.tickets as ticket>
                    <option value="${ticket.getId()}">${ticket.getTitle()}, ${ticket.getDate()}</option>
                    </#list>
                </#if>
                </select>
                <br/>
            </div>

            <div class="select-wrapper">
                <div id="new-ticket">
                    <select name="department" id="department" form="ticket-form"
                            onchange="load_ticket_enrollee()">
                <#if model.departments??>
                    <#list model.departments as department>
                        <option value="${department.getId()}">${department.getName()}</option>
                    </#list>
                <#else>
                        <option value="0">Факультетов нет</option>
                </#if>
                    </select>
                    <br/>
                </div>
            </div>
            <br/>
        </div>

        <form action="/ticket/enrollee/${model.enrollee.getId()}" method="post" role="form" id="ticket-form">
            <input type="hidden" name="id" id="id">
            <div class="row">
                <section class="6u 12u$(small)">
                    <h2>Отправитель</h2>
                    <input type="hidden" name="sender" id="sender" placeholder="Отправитель">
                    <p id="sender_field"></p>
                </section>
                <section class="6u$ 12u$(small)">
                    <h2>Получатель</h2>
                    <input type="hidden" name="receiver" id="receiver" placeholder="Получатель">
                    <p id="receiver_field"></p>
                </section>
            </div>
            <hr class="major"/>

            <div class="row">
                <section class="6u 12u$(small)">
                    <h2>Заголовок</h2>
                    <input type="text" name="title" id="title" placeholder="Заголовок">
                </section>
                <section class="6u$ 12u$(small)">
                    <h2>Содержимое</h2>
                    <input type="text" name="content" id="content" placeholder="Содержимое">
                </section>
            </div>
            <hr class="major"/>

            <div class="row">
                <section class="6u 12u$(small)">
                    <h2>Ответ</h2>
                    <input type="hidden" name="answer" id="answer" placeholder="Ответ">
                    <p id="answer_field"></p>
                </section>
                <section class="6u$ 12u$(small)">
                    <h2>Статус</h2>
                    <input type="hidden" name="status" id="status" placeholder="Статус">
                    <p id="status_field"></p>
                </section>
            </div>
            <br/>

            <header class="align-center">
                <input value="Отправить" class="button large alt" type="submit">
            </header>
        </form>

        <#include "common/copyright.ftl">

    </div>
</footer>

        <#include "common/ajax-ticket-enrollee.ftl">

    <#else>
    <h2>Извините, но вы не авторизованы. Для просмотра - сделайте это.</h2>
        <#include "common/copyright.ftl">
    </div>
</footer>
    </#if>

<#include "common/scripts.ftl">

</body>

</html>
