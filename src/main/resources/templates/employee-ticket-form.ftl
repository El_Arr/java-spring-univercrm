<!DOCTYPE HTML>
<html>

<head>
    <title>Заявка/Сотрудник</title>
    <#include "common/head.ftl">
</head>

<body class="subpage" onload="load_ticket_employee()">

<#include "common/header.ftl">

<footer id="footer">
    <div class="inner">

        <h3>Обработать заявления</h3>

        <br/>

    <#if model.tickets??>
        <div class="12u$">
            <div class="select-wrapper">
                <select name="ticket" id="ticket" onchange="load_ticket_employee()">
                    <#list model.tickets as ticket>
                        <option value="${ticket.getId()}">
                            ${ticket.getSender().getUser().getSecondName()} , ${ticket.getTitle()}
                        </option>
                    </#list>
                </select>
            </div>
        </div>
        <br/>

        <#if model.employee??>
        <h2>Состояние</h2>
        <select name="status" id="status" form="ticket-form">
            <#list model.statuses as status>
                <option value="${status}">${status}</option>
            </#list>
        </select>
        <br/>
        <form action="/ticket/employee/${model.employee.getId()}" method="post" role="form" id="ticket-form">
            <div class="row">
                <input type="hidden" name="id" id="id">
                <section class="6u 12u$(small)">
                    <h2>Отправитель</h2>
                    <input type="hidden" name="sender" id="sender" placeholder="Отправитель">
                    <p id="sender_field"></p>
                </section>
                <section class="6u$ 12u$(small)">
                    <h2>Получатель</h2>
                    <input type="hidden" name="receiver" id="receiver" placeholder="Получатель">
                    <p id="receiver_field"></p>
                </section>
            </div>
            <hr class="major"/>

            <div class="row">
                <section class="6u 12u$(small)">
                    <h2>Заголовок</h2>
                    <input type="hidden" name="title" id="title" placeholder="Заголовок">
                    <p id="title_field"></p>
                </section>
                <section class="6u$ 12u$(small)">
                    <h2>Содержимое</h2>
                    <input type="hidden" name="content" id="content" placeholder="Содержимое">
                    <p id="content_field"></p>
                </section>
            </div>
            <hr class="major"/>

            <h2>Ваш ответ</h2>
            <textarea name="answer" id="answer" rows="6" placeholder="Ваш ответ"></textarea>
            <br/>

            <header class="align-center">
                <input value="Отправить" class="button large alt" type="submit">
            </header>
        </form>
        <#else>
            <h2>Извините, но вы не авторизованы. Для просмотра - сделайте это.</h2>
        </#if>
    <#else>
        <h2>Вам не поступало никаких заявок!</h2>
    </#if>
        <#include "common/copyright.ftl">

    </div>
</footer>

<#include "common/ajax-ticket-employee.ftl">

<#include "common/scripts.ftl">

</body>

</html>
