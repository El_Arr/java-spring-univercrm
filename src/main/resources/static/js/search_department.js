function search_department() {
    $.ajax({
        data: {"q": $("#q_id").val()},
        dataType: 'json',
        url: "/department-search",
        type: 'POST',
        success: function (result) {
            $("#results").html("");
            for (var i = 0; i < result[1].length; i++) {
                $("#results").append("" +
                    "<div class='row'>" +
                    "   <section class='6u 12u$(medium)'>" +
                    "       <h2>" + result[1][i] + "</h2>" +
                    "       <p><strong>" + result[1][i] + "</strong> - это " + result[2][i] + "</p>" +
                    "   </section>" +
                    "   <section class='3u 6u(medium) 12u$(small)'>" +
                    "       <h3>Контакты</h3>" +
                    "       <p>Email: " + result[3][i] + "</p>" +
                    "       <p>Телефон: " + result[4][i] + "</p>" +
                    "   </section>" +
                    "   <section class='3u$ 6u$(medium) 12u$(small)'>" +
                    "       <h3>Узнать больше:</h3>" +
                    "       <a href='/department/" + result[0][i] + "' class='button'>" + result[1][i] + "</a>" +
                    "   </section>" +
                    "</div>" +
                    "<hr class='major'/>")
            }
        },
        error: function (e) {
            console.log("error: " + e);
        }
    });
}