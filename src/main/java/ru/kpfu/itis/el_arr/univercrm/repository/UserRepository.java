package ru.kpfu.itis.el_arr.univercrm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.el_arr.univercrm.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByFirstNameAndSecondName(String firstName, String secondName);

}
