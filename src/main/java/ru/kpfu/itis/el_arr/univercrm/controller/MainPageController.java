package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;
import ru.kpfu.itis.el_arr.univercrm.service.EventService;

@Controller
public class MainPageController {
    private EventService eventService;
    private EmployeeService employeeService;

    public MainPageController(EventService eventService, EmployeeService employeeService) {
        this.eventService = eventService;
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMainPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("events", eventService.getAllEvents());
        model.addAttribute("Khasik", employeeService.getEmployeeById(2));
        model.addAttribute("Gafurov", employeeService.getEmployeeById(1));
        model.addAttribute("user", authentication != null);
        return "main";
    }
}
