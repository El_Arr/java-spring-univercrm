package ru.kpfu.itis.el_arr.univercrm.service;

import ru.kpfu.itis.el_arr.univercrm.model.Ticket;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserRole;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;

import java.util.List;

public interface UserInfoService {

    void addUser(String username, String password, String first_name, String second_name,
                 int age, String contact, UserRole role, UserStatus status);

    void removeUser(User user);

    void removeUserById(long id);

    void editUser(long id, String role, String status, String first_name, String second_name,
                  int age, String contact, List<Ticket> ticketsRecieved, List<Ticket> ticketsSent);

    void editUser(long id, String first_name, String second_name, int age, String contact);

    void editUserTickets(User user, List<Ticket> ticketsReceived, List<Ticket> ticketsSent);

    List<User> getAllUsers();

    User getUserById(long id);

    User getUserByFullName(String firstName, String secondName);

    User getUserByUsername(String username);

    List<UserLoginData> getAllUsersLoginData();

    UserLoginData getUserLoginDataByUserId(long id);

    UserLoginData getUserLoginDataByUsername(String username);

}
