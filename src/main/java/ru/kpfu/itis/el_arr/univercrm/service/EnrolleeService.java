package ru.kpfu.itis.el_arr.univercrm.service;

import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;

import java.io.IOException;
import java.util.List;

public interface EnrolleeService {

    void addEnrollee(String first_name, String second_name, MultipartFile file);

    void removeEnrolleeById(long id);

    void removeEnrollee(Enrollee enrollee);

    void editEnrollee(long id, MultipartFile file) throws IOException;

    List<Enrollee> getAllEnrollees();

    Enrollee getEnrolleeById(long id);

    Enrollee getEnrolleeByUserId(long id);

}
