package ru.kpfu.itis.el_arr.univercrm.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterForm {
    private String username;
    private String password;
    private String first_name;
    private String second_name;
    private int age;
    private String contact;
    private String role;
    private String job_description;
    private String department_id;
}
