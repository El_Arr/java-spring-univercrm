package ru.kpfu.itis.el_arr.univercrm.security.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserStatus;
import ru.kpfu.itis.el_arr.univercrm.repository.UserLoginDataRepository;

import java.util.Collection;
import java.util.Optional;

@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {
    private UserLoginDataRepository userLoginDataRepository;
    private UserDetailsService userDetailsService;
    private PasswordEncoder encoder;

    @Autowired
    public AuthenticationProviderImpl(PasswordEncoder encoder, UserLoginDataRepository userLoginDataRepository,
                                      @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService) {
        this.userLoginDataRepository = userLoginDataRepository;
        this.userDetailsService = userDetailsService;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Optional<UserLoginData> userLoginDataOptional = userLoginDataRepository.findByUsername(username);
        if (userLoginDataOptional.isPresent()) {
            UserLoginData userLoginData = userLoginDataOptional.get();
            if (encoder.matches(password, userLoginData.getPasswordHashed())) {
                if (userLoginData.getStatus().equals(UserStatus.BANNED)) {
                    throw new BadCredentialsException("Confirmation is failed");
                }
            } else {
                throw new BadCredentialsException("Invalid login or password");
            }
        } else {
            throw new BadCredentialsException("Such user doesn't exist!");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        Collection<? extends GrantedAuthority> grantedAuthorities = userDetails.getAuthorities();
        return new UsernamePasswordAuthenticationToken(userDetails, password, grantedAuthorities);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

}
