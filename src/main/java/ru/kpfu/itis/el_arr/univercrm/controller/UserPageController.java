package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.Enrollee;
import ru.kpfu.itis.el_arr.univercrm.model.UserLoginData;
import ru.kpfu.itis.el_arr.univercrm.service.DepartmentService;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;
import ru.kpfu.itis.el_arr.univercrm.service.EnrolleeService;
import ru.kpfu.itis.el_arr.univercrm.service.UserInfoService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public class UserPageController {
    private DepartmentService departmentService;
    private UserInfoService userInfoService;
    private EnrolleeService enrolleeService;
    private EmployeeService employeeService;

    public UserPageController(DepartmentService departmentService, UserInfoService userInfoService,
                              EnrolleeService enrolleeService, EmployeeService employeeService) {
        this.departmentService = departmentService;
        this.userInfoService = userInfoService;
        this.enrolleeService = enrolleeService;
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String getUserPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        if (authentication != null) {
            Collection<? extends GrantedAuthority> grantedAuthorities = authentication.getAuthorities();
            for (GrantedAuthority authority : grantedAuthorities) {
                switch (authority.getAuthority()) {
                    case "ADMIN":
                        return "redirect:/admin";
                    case "DIRECTOR":
                        return "redirect:/director";
                    case "EMPLOYEE":
                        return "redirect:/employee/me";
                    case "ENROLLEE":
                        return "redirect:/enrollee/me";
                }
            }
        }
        model.addAttribute("user", false);
        return "redirect:/options";
    }

    @RequestMapping(value = "/enrollee/me", method = RequestMethod.GET)
    public String showMyEnrolleePage(Authentication authentication) {
        Enrollee enrollee = enrolleeService.getEnrolleeByUserId(userInfoService.getUserByUsername(authentication.getName()).getId());
        return "redirect:/enrollee/" + enrollee.getId();
    }

    @RequestMapping(value = "/enrollee/{id}", method = RequestMethod.GET)
    public String showEnrolleePage(@ModelAttribute("model") ModelMap model,
                                   @PathVariable("id") String id,
                                   Authentication authentication) {
        Enrollee enrollee = enrolleeService.getEnrolleeById(Long.valueOf(id));
        UserLoginData loginData = userInfoService.getUserLoginDataByUserId(enrollee.getUser().getId());
        Boolean me = true;
        if (loginData.getId() != userInfoService.getUserLoginDataByUsername(authentication.getName()).getId()) {
            me = false;
        }
        model.addAttribute("meme", me);
        model.addAttribute("enrollee", enrollee);
        model.addAttribute("userLoginData", loginData);
        model.addAttribute("user", true);
        return "enrollee";
    }

    @RequestMapping(value = "/enrollee/{id}/edit", method = RequestMethod.GET)
    public String showEditEnrolleePage(@ModelAttribute("model") ModelMap model, @PathVariable("id") String id) {
        model.addAttribute("enrollee", enrolleeService.getEnrolleeById(Long.valueOf(id)));
        model.addAttribute("user", true);
        return "enrollee-edit";
    }

    @RequestMapping(value = "/enrollee/{id}/edit-successful", method = RequestMethod.POST)
    public String redirectEnrolleeAfterSuccess(@PathVariable("id") String id,
                                               @RequestParam("file") MultipartFile file) throws IOException {
        enrolleeService.editEnrollee(
                Long.valueOf(id), file
        );
        return "redirect:/enrollee/{id}";
    }

    @RequestMapping(value = "/enrollee/{id}/diploma", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody
    byte[] imageUrl(@PathVariable("id") String id) throws IOException {
        return enrolleeService.getEnrolleeById(Long.valueOf(id)).getDiploma();
    }

    @RequestMapping(value = "/employee/me", method = RequestMethod.GET)
    public String showMyEmployeePage(Authentication authentication) {
        Employee employee = employeeService.getEmployeeByUserId(userInfoService.getUserByUsername(authentication.getName()).getId());
        return "redirect:/employee/" + employee.getId();
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public String showEmployeePage(@ModelAttribute("model") ModelMap model,
                                   @PathVariable("id") String id,
                                   Authentication authentication) {
        Employee employee = employeeService.getEmployeeById(Long.valueOf(id));
        UserLoginData loginData = userInfoService.getUserLoginDataByUserId(employee.getUser().getId());
        if (authentication != null) {
            Boolean me = false;
            if (loginData.getId() == userInfoService.getUserLoginDataByUsername(authentication.getName()).getId()) {
                me = true;
            }
            model.addAttribute("meme", me);
        }
        model.addAttribute("employee", employee);
        model.addAttribute("userLoginData", loginData);
        model.addAttribute("user", true);
        return "employee";
    }

    @RequestMapping(value = "/employee/{id}/edit", method = RequestMethod.GET)
    public String showEditEmployeePage(@ModelAttribute("model") ModelMap model, @PathVariable("id") String id) {
        model.addAttribute("employee", employeeService.getEmployeeById(Long.valueOf(id)));
        model.addAttribute("departments", departmentService.getAllDepartments());
        model.addAttribute("user", true);
        return "employee-edit";
    }

    @RequestMapping(value = "/employee/{id}/edit-successful", method = RequestMethod.POST)
    public String redirectEmployeeAfterSuccess(@PathVariable("id") String id,
                                               @RequestParam String first_name,
                                               @RequestParam String second_name,
                                               @RequestParam String age,
                                               @RequestParam String contact,
                                               @RequestParam String job_description,
                                               @RequestParam String department_id) {
        employeeService.editEmployee(
                Long.valueOf(id), first_name, second_name,
                age, contact, departmentService.getDepartmentById(Long.valueOf(department_id)), job_description
        );
        return "redirect:/employee/{id}";
    }

    @RequestMapping(value = "/user-search", method = RequestMethod.POST)
    @ResponseBody
    public List<String> ajaxUserSearch(@RequestParam(value = "id") String id) {
        UserLoginData user = userInfoService.getUserLoginDataByUserId(Long.valueOf(id));
        List<String> result = new ArrayList<>();
        result.add(user.getStatus().name());
        result.add(user.getRole().name());
        return result;
    }

}
