package ru.kpfu.itis.el_arr.univercrm.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ContactsPageController {

    public ContactsPageController() {}

    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public String getContactsPage(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        model.addAttribute("user", authentication != null);
        return "contacts";
    }
}
