package ru.kpfu.itis.el_arr.univercrm.service.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.el_arr.univercrm.model.Department;
import ru.kpfu.itis.el_arr.univercrm.model.Speciality;
import ru.kpfu.itis.el_arr.univercrm.repository.DepartmentRepository;
import ru.kpfu.itis.el_arr.univercrm.repository.SpecialityRepository;
import ru.kpfu.itis.el_arr.univercrm.service.SpecialityService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SpecialityServiceImpl implements SpecialityService {
    private SpecialityRepository specialityRepository;
    private DepartmentRepository departmentRepository;

    public SpecialityServiceImpl(SpecialityRepository specialityRepository,
                                 DepartmentRepository departmentRepository) {
        this.specialityRepository = specialityRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public void addSpeciality(long dept_id, String main_description,
                              String small_description_1, String small_description_2) {
        Department department = departmentRepository.getOne(dept_id);
        specialityRepository.save(
                Speciality.builder()
                        .department(department)
                        .main_description(main_description)
                        .small_description_1(small_description_1)
                        .small_description_2(small_description_2)
                        .build()
        );
    }

    @Override
    public void removeSpecialityById(long id) {
        specialityRepository.deleteById(id);
    }

    @Override
    public List<Speciality> getAllSpecialities() {
        return specialityRepository.findAll();
    }

    @Override
    public List<Speciality> getAllSpecialitiesByDepartmentId(long id) {
        return specialityRepository.findAll().stream()
                .filter(o -> o.getDepartment().getId() == id)
                .collect(Collectors.toList());
    }

    @Override
    public List<Speciality> getSpecialitiesByNamePartAndDepartmentId(String q, long id) {
        return departmentRepository.getOne(id).getSpecialities()
                .stream()
                .filter(o -> o.getName().toLowerCase().contains(q.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public Speciality getSpecialityById(long id) {
        return specialityRepository.findById(id).orElse(null);
    }

}
