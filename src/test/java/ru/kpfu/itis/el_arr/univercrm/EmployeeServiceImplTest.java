package ru.kpfu.itis.el_arr.univercrm;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kpfu.itis.el_arr.univercrm.model.Employee;
import ru.kpfu.itis.el_arr.univercrm.model.User;
import ru.kpfu.itis.el_arr.univercrm.model.constant.UserRole;
import ru.kpfu.itis.el_arr.univercrm.repository.EmployeeRepository;
import ru.kpfu.itis.el_arr.univercrm.service.DepartmentService;
import ru.kpfu.itis.el_arr.univercrm.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UniverCRMApplication.class)
public class EmployeeServiceImplTest {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Before
    public void setUp() {
        Employee employee1 = Employee.builder()
                .id(1L)
                .job_description("Good guy 1")
                .department(departmentService.getDepartmentById(1))
                .user(
                        User.builder()
                                .id(11L)
                                .secondName("Vasiliyev")
                                .firstName("Vasily")
                                .age(18)
                                .role(UserRole.EMPLOYEE)
                                .contact("Loli@mail.ru")
                                .build()
                ).build();
        Employee employee2 = Employee.builder()
                .id(2L)
                .job_description("Good guy 2")
                .department(departmentService.getDepartmentById(1))
                .user(
                        User.builder()
                                .id(12L)
                                .secondName("Petrov")
                                .firstName("Pyotr")
                                .age(18)
                                .role(UserRole.EMPLOYEE)
                                .contact("Loli@mail.ru")
                                .build()
                ).build();
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(employee1);
        employees.add(employee2);
        Mockito.when(employeeRepository.findById(employee1.getId())).thenReturn(Optional.of(employee1));
        Mockito.when(employeeRepository.findAll()).thenReturn(employees);
    }

    @Test
    public void whenGetEmployeeById_thenReturnEmployee() {
        Long id = 1L;
        Employee found = employeeService.getEmployeeById(id);
        assertThat(found.getId()).isEqualTo(id);
    }

    @Test
    public void whenGetAllEmployees_thenReturnAllEmployeeList() {
        List<Employee> foundEmployees = employeeService.getAllEmployees();
        assertThat(foundEmployees.size()).isEqualTo(2);
    }

    @Test
    public void whenGetEmployeesByDeptID_thenReturnAllEmployeesList() {
        List<Employee> employees = employeeService.getEmployeesByDeptId(1);
        assertThat(employees.size()).isEqualTo(2);
        assertThat(employees.get(0).getUser().getRole()).isEqualTo(UserRole.EMPLOYEE);
    }

    @Test
    public void whenGetEmployeeByUserId_thenReturnEmployeeUser() {
        Long id = 11L;
        Employee found = employeeService.getEmployeeByUserId(id);
        assertThat(found.getUser().getId()).isEqualTo(id);
    }

}
