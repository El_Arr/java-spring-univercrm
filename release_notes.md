# UniverCRM

## Changes: 

* Minor changes to controllers and header template 
	* Now 'User', 'Login' and 'Logout' buttons handled the right way if user isn't logged in
* Department search field fixed
	* Now filters list instead of making new one from simple links
* Specialities search field added
	* Now filters list, as dep. search
* StringTrimmer removed with usages 
    * Shitty class ~ quirk
* JS functions are now in separate files
    * Easier to change logic
* Template error handling added
    * No more error pages when:
        * Searching for departments and specialities
        * Sending tickets

> by El_Arr